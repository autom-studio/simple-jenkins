# 在本地快速部署 Jenkins

## 环境准备

准备一个安装有 docker 和 docker-compose 的主机即可。

## 组成部分

+ master 接收 webhook ，调度任务等
+ agent 执行构建任务
+ dind 构建镜像

## 开始部署

### 部署 master

+ 修改持久化目录属主为 1000:1000

```
cd jenkins/master
chown -R 1000:1000 jenkins_home
chown -R 1000:1000 init.groovy.d
```

+ 执行 docker-compose 命令完成部署

创建 docker network

```
docker network create -d bridge jenkins-cluster
```

启动

```
docker-compose up -d
```

+ 完成初始化并安装插件

blueocean 镜像已包含常用插件，在首次访问 Jenkins 时，选择安装推荐插件即可。

初始化完成后，登录 Jenkins ，进入“系统管理->插件管理->可选插件” 在搜索框搜索 “gitee” ，选中 gitee 插件安装。

![install jenkins plugin 1](./imgs/find_gitee_plugin_1.png)

![install jenkins plugin 2](./imgs/find_gitee_plugin_2.png)

+ 配置 Gitee 插件

完成 gitee 插件安装后，进入“系统管理->系统配置”，定位到 “Gitee 配置” 部分，创建证书令牌（需要先在 [Gitee](https://gitee.com/profile/personal_access_tokens) 创建 Gitee API V5 的私人令牌），并完成配置。

![config gitee plugin](./imgs/config_gitee_plugin.png)

+ 新建 agent

进入 “系统管理->节点列表” ，点击新建节点

创建两个“固定节点”，节点配置如下

![add agent 1](./imgs/add_agent_1.png)

![add agent 2](./imgs/add_agent_2.png)

![add agent 3](./imgs/add_agent_3.png)

![add agent 4](./imgs/add_agent_4.png)

创建完成后，点击节点，查看节点连接信息，部署 agent 时需要提供该连接信息

![add agent 5](./imgs/add_agent_5.png)

### 部署 agent

+ 构建自定义 agent 镜像

下载 kubectl 、 docker 、 helm 二进制文件

```
cd jenkins/agent/bin
curl -LO "https://storage.googleapis.com/kubernetes-release/release/v1.21.6/bin/linux/amd64/kubectl"
# 或者从 https://cowtransfer.com/s/564949f6d62b43 下载
```

准备 .kube 和 .docker 文件夹，用于授权访问 kubernetes api-server 和 harbor 镜像仓库

`.kube/config` 从 kubernetes 集群内获取

创建 `.docker/config.json`

```
harbor_addr=hub.atompi.cc
harbor_username=admin
harbor_password=admin123
auth_str=$(echo -ne "$harbor_username:$harbor_password" | base64)
cat > .docker/config.json <<EOF
{
        "auths": {
                "$harbor_addr": {
                        "auth": "$auth_str"
                }
        }
}
EOF
```

docker build agent 镜像

```
docker build -t $harbor_addr/jenkins/inbound-agent:4.11-1-kube .
docker push $harbor_addr/jenkins/inbound-agent:4.11-1-kube
```

+ 执行 docker-compose 命令完成部署

修改 docker-compose.yml 中的 secret 个 agent_name ，对应于上面 master 中创建的节点连接信息

```
docker-compose up -d
```

### 部署 dind

+ 创建 daemon.json

```
cd dind
harbor_addr=hub.atompi.cc
dns_addr=172.18.0.39
cat > etc/daemon.json <<EOF
{
  "registry-mirrors": ["https://gfty7g09.mirror.aliyuncs.com"],
  "insecure-registries": ["$harbor_addr"],
  "live-restore": true,
  "default-shm-size": "128M",
  "max-concurrent-downloads": 10,
  "oom-score-adjust": -1000,
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "dns": [
    "$dns_addr"
  ]
}
EOF
```

+ 执行 docker-compose 命令完成部署

```
docker-compose up -d
```

## 完成
